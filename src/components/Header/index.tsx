import {View,StyleSheet,Text,StatusBar, TouchableOpacity, Button} from 'react-native'
import React from 'react'
import {Feather} from '@expo/vector-icons'
import { resetNavigation } from '../../../App'
export default function Header(){
    return (
        <View style={styles.container}>
            <Button title='Clique para limoar' onPress={()=>{resetNavigation()}}/>
        </View>
    )
}

const statusBarHeight = StatusBar.currentHeight ? StatusBar.currentHeight+22 : 64;

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#176ea4',
        paddingTop:statusBarHeight
    }
})