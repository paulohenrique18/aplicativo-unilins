import { api } from "../../Resources/networkConfig"

export default async function getCurrentUserSelecao(){
    const url = '/getSelecao'
    try{
        const result = await api.get(url);
        const headers = result.headers;
        api.interceptors.request.use(config => {
            config.headers['EduContextoAlunoResponsavelAPI']=headers['educontextoalunoresponsavelapi']
            return config;
        });
    }catch(error){

    }

}