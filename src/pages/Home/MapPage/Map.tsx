import {TouchableOpacity, View,Image,StyleSheet} from "react-native";

export default function Map(){
    return (
        <View style={styles.container}>
            <TouchableOpacity activeOpacity={0.7}>
                <Image
                    source={require('../../../../assets/mapa_campus-04-22-1-scaled-2048x1228.jpg')}
                    style={{ width: 600, height: 600 }}
                    resizeMode='contain'
                />
            </TouchableOpacity>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
});