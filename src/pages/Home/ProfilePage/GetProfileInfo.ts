import { api } from "../../../Resources/networkConfig";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default async function GetProfileInfo(){
    const profileUrl = '/getCurrentUser/context';
    const cacheKey = "profile_info";
    const cacheDuration = 3600;
    try{
        const cachedProfileInfo = await AsyncStorage.getItem(cacheKey);
        if(cachedProfileInfo!==null){
            const {data,expires} = JSON.parse(cachedProfileInfo);
            if(Date.now()<expires){
                console.log("Informacoes do perfil recuperadas do cache!");
                return data;
            }else{
                console.log("Dados do cache expirados!");
                await AsyncStorage.removeItem(cacheKey);
            }
        }
        const result = await api.get(profileUrl);
        const data = result.data;
        const cacheData = {
            data,
            expires:Date.now()+cacheDuration*1000,
        }
        await AsyncStorage.setItem(cacheKey,JSON.stringify(cacheData));
        console.log("Dados do perfil armazenados no cache!");
        return data;
    }catch(e){

    }
}