import React, {useEffect, useLayoutEffect, useState} from "react";
import {
    ActivityIndicator,
    View,
    StyleSheet,
    Text,
    Image,
    ImageSourcePropType,
    TouchableOpacity,
    StatusBar
} from "react-native";
import GetProfileInfo from "./GetProfileInfo";
import { useNavigation } from "@react-navigation/native";
type ProfileInfo = {
    apresentacao: string;
    fotoAluno: string;
    nomeAluno: string;
    nomeCurso: string;
}
export default function Profile() {
    const navigation = useNavigation();
    navigation.setOptions({
        headerStyle: {
            backgroundColor: '#176ea4',
        },
        headerTitleStyle:{
            color:"white"
        }
    })
    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <View>
                    <Image
                        source={require('../../../../assets/logotipo_UNILINS-U.png')}
                        style={{ width: 30, height: 34,marginRight:10 }}
                    />
                </View>
            ),
            title:'Perfil'
        });
    }, [navigation]);
    const [profile, setProfile] = useState<ProfileInfo>({
        apresentacao: "",
        fotoAluno: "",
        nomeAluno: "",
        nomeCurso: ""
    })
    const [isLoading, setIsLoading] = useState(false);
    const getProfileInfo = async () => {
        setIsLoading(true);
        const result = await GetProfileInfo()
        const apresentacao = result['APRESENTACAO'];
        const fotoAluno = result['FOTOALUNO'];
        const nomeAluno = result['NOMEALUNO'];
        const nomeCurso = result['NOMECURSO'];
        const profile: ProfileInfo = {
            apresentacao,
            fotoAluno,
            nomeAluno,
            nomeCurso
        }
        setProfile(profile);
        setIsLoading(false);
    }

    useEffect(() => {
        getProfileInfo()
            .then(r => {})
    }, [])

    return (
        <View style={styles.container}>
            <StatusBar
                barStyle={'light-content'}
                backgroundColor={'#176ea4'}
            />
            {
                isLoading ?
                    <View>
                        <ActivityIndicator size={50} color="white" style={{ marginTop: 30 }} />
                    </View>
                    :
                    <View style={styles.personalInfo}>
                        <Image
                            source={require('../../../../assets/fds.png')}
                            style={{ width: 200, height: 50 }}
                        />
                        <Text style={styles.name}>{profile.nomeAluno}</Text>
                        <Image 
                            source={{uri:`data:image/png;base64,${profile.fotoAluno}`}} 
                            style={{ width: 200, height: 200 }}
                        />
                        <Text style={styles.name}>Curso: {profile.nomeCurso}</Text>
                    </View>
            }
        </View>
    )
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"#2f3330",
        alignItems:"center",
        justifyContent:"center",
        paddingBottom:100
    },
    personalInfo:{
        flex:1,
        alignItems:"center",
        textAlign:"center",
        justifyContent:"space-evenly"
    },
    name:{
        fontSize:20,
        color:"white"
    }
})