export type ClasseObject = {
    BLOCO:string;
    CODCOLIGADA:number;
    NOME:string;
    PREDIO:string;
    SALA:string;
    TIPO:string;
}
export type ClasseTime = {
    DIASEMANA:string;
    NOME:string;
    SALA:string;
    HORAINICIAL:string;
    HORAFINAL:string;
}
export type ClassesResponse = {
    SDescDisc:ClasseObject[];
    SHorarioAluno:ClasseTime[];
};