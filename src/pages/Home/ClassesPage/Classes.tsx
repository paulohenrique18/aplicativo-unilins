import {ActivityIndicator, StyleSheet, View, Text, FlatList, Image, StatusBar} from "react-native";
import {useEffect, useLayoutEffect, useState} from "react";
import getUserClasses from "./GetUserClasses";
import {ClasseObject, ClassesResponse, ClasseTime} from "./ClassesTypes";
import React from "react";
import {useNavigation} from "@react-navigation/native";
const handleWeekDay = (diaSemana:string)=>{
    switch (diaSemana) {
        case '2':return "Segunda";
        case '3':return "Terça";
        case '4':return "Quarta";
        case '5':return "Quinta";
        case '6':return "Sexta";
        case '7':return "Sábado";
        case '8':return "Domingo";
        default:return "Error!"
    }
}
const TableItem = ({ index, classe }: { index: number, classe: ClasseTime }):any => {
    const weekDay = handleWeekDay(classe.DIASEMANA);
    const diasDaSemana = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
    const dataAtual = new Date();
    const diaDaSemana = diasDaSemana[dataAtual.getDay()];
    if(weekDay==='Error!'){
        return;
    }
    return (
        <View style={{...styles.tableCell,backgroundColor:diaDaSemana===weekDay?"red":""}}>
            <Text style={{width:100,color:"white",fontSize:20,paddingLeft:10}}>{weekDay}</Text>
            <View style={{flexDirection: 'column', padding: 5 }}>
                <Text style={{color:"white"}}>Nome: {classe.NOME}</Text>
                <Text style={{color:"white"}}>Sala: {classe.SALA}</Text>
                <Text style={{color:"white"}}>Dia: {weekDay}</Text>
                <Text style={{color:"white"}}>Horario: {classe.HORAINICIAL}h-{classe.HORAFINAL}h</Text>
            </View>
        </View>
    );
};
export default function Classes(){
    const navigation = useNavigation();
    navigation.setOptions({
        headerStyle: {
            backgroundColor: '#176ea4',
        },
        headerTitleStyle:{
            color:"white"
        }
    })
    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <View>
                    <Image
                        source={require('../../../../assets/logotipo_UNILINS-U.png')}
                        style={{ width: 30, height: 34,marginRight:10 }}
                    />
                </View>
            ),
            title:'Aulas'
        });
    }, [navigation]);
    const [classes,setClasses]=useState<ClassesResponse>()
    const [isLoading,setIsLoading]=useState(false);
    const handleGetClasses = async ()=>{
        setIsLoading(true);
        const result = await getUserClasses();
        if(result&&result.SDescDisc.length>0){
            console.log(result)
            setIsLoading(false)
            setClasses(result);
        }
    }

    useEffect(()=>{
        handleGetClasses();
    },[])

    return (
        <View style={styles.container}>
            <StatusBar
                barStyle={'light-content'}
                backgroundColor={'#176ea4'}
            />
            {
                isLoading?
                    <View>
                        <ActivityIndicator size={50} color="white" style={{ marginTop: 30 }} />
                    </View>
                    :
                    <View>
                        <FlatList
                            data={classes?.SHorarioAluno
                                .splice(5)
                                .sort((a,b)=>parseInt(a.DIASEMANA)-parseInt(b.DIASEMANA))}
                            keyExtractor={(item) => item.NOME+item.DIASEMANA}
                            renderItem={({ item, index }) => <TableItem classe={item} index={index} />}
                        />
                    </View>
            }
        </View>
    )
}


const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: "center",
        backgroundColor: "#2f3330",
    },
    tableCell:{
        flexDirection:"row",
        alignItems:"center",
        borderWidth:2,
        borderColor:"white",
        borderRadius:15,
        margin:5,
        marginTop:10,
        padding:5
    }
})