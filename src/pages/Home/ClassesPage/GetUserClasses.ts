import { api } from "../../../Resources/networkConfig";
import { ClassesResponse } from "./ClassesTypes";
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function getUserClasses(){

    const url = "/getClasses"
    const cacheKey = 'user_classes'; // Chave do item no cache
    const cacheDuration = 3600; // Duração do cache em segundos (1 hora)

    try {

        const cachedClasses = await AsyncStorage.getItem(cacheKey);
        if (cachedClasses !== null) {
            const { data, expires } = JSON.parse(cachedClasses);
            if (Date.now() < expires) {
                console.log('Classes obtidas do cache');
                return data as ClassesResponse;
            } else {
                console.log('Dados do cache expirados');
                await AsyncStorage.removeItem(cacheKey);
            }
        }

        console.log('Buscando classes na API');

        const result = await api.get<ClassesResponse>(url);
        const classes = result.data;
        const cacheData = {
            data: classes,
            expires: Date.now() + cacheDuration * 1000, // adiciona a duração do cache em milissegundos
        };
        await AsyncStorage.setItem(cacheKey, JSON.stringify(cacheData));
        console.log('Classes armazenadas em cache');
        return classes;
    } catch (e) {
        console.log('Erro ao buscar classes: ', e);
    }
}
