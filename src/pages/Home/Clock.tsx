import { useEffect, useState } from "react";
import { View,Text, TextProps } from "react-native";
interface ClockProps{
    style:{}
}
export default function Clock(props:ClockProps) {
    const [time, setTime] = useState(new Date());

    useEffect(() => {
        const intervalId = setInterval(() => {
            setTime(new Date());
        }, 1000);

        return () => {
            clearInterval(intervalId);
        };
    }, []);

    return (
        <>
            <Text style={props.style}>{time.toLocaleTimeString()}</Text>
        </>
    );
}