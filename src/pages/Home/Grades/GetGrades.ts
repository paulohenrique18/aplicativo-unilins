import AsyncStorage from "@react-native-async-storage/async-storage";
import {api} from "../../../Resources/networkConfig";

export default async function GetGrades(){

    const profileUrl = '/getGrades';
    const cacheKey = "grades_info";
    const cacheDuration = 3600;
    try{
        const cachedProfileInfo = await AsyncStorage.getItem(cacheKey);
        if(cachedProfileInfo!==null){
            const {data,expires} = JSON.parse(cachedProfileInfo);
            if(Date.now()<expires){
                console.log("Informacoes de notas recuperadas do cache!");
                return data;
            }else{
                console.log("Dados de notas expirados!");
                await AsyncStorage.removeItem(cacheKey);
            }
        }
        const result = await api.get(profileUrl);
        const data = result.data;
        const cacheData = {
            data,
            expires:Date.now()+cacheDuration*1000,
        }
        await AsyncStorage.setItem(cacheKey,JSON.stringify(cacheData));
        console.log("Dados de notas armazenados no cache!");
        return data;
    }catch(e){

    }



}