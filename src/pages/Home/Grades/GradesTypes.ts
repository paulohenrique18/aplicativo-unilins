export type Disciplina = {
    "1 - P1": string | null,
    "2 - P2": string | null,
    "3 - Sub": string | null,
    "5 - G1": string | null,
    "6 - G2": string | null,
    "7 - MA": string | null,
    "8 - MF": string | null,
    "CODDISC": string,
    "CODTURMA": string,
    "DISCIPLINA": string,
    "FILIAL": string,
    "IDTURMADISC": number,
    "SITUACAO": string,
};
export const InitialDisciplinaValue:Disciplina = {
    "1 - P1": "",
    "2 - P2": "",
    "3 - Sub": "",
    "5 - G1": "",
    "6 - G2": "",
    "7 - MA": "",
    "8 - MF": "",
    CODDISC: "",
    CODTURMA: "",
    DISCIPLINA: "",
    FILIAL: "",
    IDTURMADISC: 0,
    SITUACAO: ""

}