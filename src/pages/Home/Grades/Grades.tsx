import {ActivityIndicator, View, Text, StyleSheet, ScrollView, Image, StatusBar} from "react-native";
import React, {useEffect, useLayoutEffect, useState} from "react";
import GetGrades from "./GetGrades";
import {Disciplina, InitialDisciplinaValue} from "./GradesTypes";
import {useNavigation} from "@react-navigation/native";

export default function Grades(){
    const [isLoading,setIsLoading]=useState(true);
    const [grades,setGrades]=useState<Disciplina[]>([InitialDisciplinaValue]);
    const navigation = useNavigation();
    navigation.setOptions({
        headerStyle: {
            backgroundColor: '#176ea4',
        },
        headerTitleStyle:{
            color:"white"
        }
    });
    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <View>
                    <Image
                        source={require('../../../../assets/logotipo_UNILINS-U.png')}
                        style={{ width: 30, height: 34,marginRight:10 }}
                    />
                </View>
            ),
            title:'Notas',
        });
    }, [navigation]);
    const handleGetGrades = async () =>{
        const result = await GetGrades();
        console.log(result);
        setIsLoading(false);
        setGrades(result);
    }

    useEffect(()=>{
        handleGetGrades();
    },[])

    return (
        <ScrollView style={{backgroundColor:'#2f3330'}}>
            <StatusBar
                barStyle={'light-content'}
                backgroundColor={'#176ea4'}
            />
        {
            isLoading?
                <View>
                    <ActivityIndicator size={50} color="white" style={{ marginTop: 30 }} />
                </View>
                :
                <View>
                    <Tabela dados={grades}/>
                </View>
        }
        </ScrollView>
    )
}
const Tabela = ({ dados }:{dados:Disciplina[]}) => {
    return (
        <>
            {dados.map((item, index:number) => (
                <View style={styles.container} key={index}>
                    <Text style={styles.title}>{item.DISCIPLINA}</Text>

                    <View style={styles.row}>
                        <Text style={styles.cell}>1 - P1</Text>
                        <Text style={styles.cell}>{item["1 - P1"] || "-"}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.cell}>2 - P2</Text>
                        <Text style={styles.cell}>{item["2 - P2"] || "-"}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.cell}>3 - Sub</Text>
                        <Text style={styles.cell}>{item["3 - Sub"] || "-"}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.cell}>5 - G1</Text>
                        <Text style={styles.cell}>{item["5 - G1"] || "-"}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.cell}>6 - G2</Text>
                        <Text style={styles.cell}>{item["6 - G2"] || "-"}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.cell}>7 - MA</Text>
                        <Text style={styles.cell}>{item["7 - MA"] || "-"}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.cell}>8 - MF</Text>
                        <Text style={styles.cell}>{item["8 - MF"] || "-"}</Text>
                    </View>
                </View>
            ))}
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        borderWidth: 5,
        borderColor: '#176ea4',
        borderRadius: 15,
        padding: 5,
        marginVertical: 10,
        margin:15
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16,
        color:"white",
        textAlign:"center",
        backgroundColor:"#176ea4",
        padding:8,
        borderRadius:15
    },
    subtitle: {
        fontSize: 14,
        color:"white",
        textAlign:"center"
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 2,
        paddingTop:10
    },
    cell: {
        flex: 1,
        marginHorizontal: 2,
        color:"white"
    },
});
