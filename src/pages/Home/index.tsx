
import {StyleSheet, Text, View, Image, ImageBackground, StatusBar} from 'react-native';

import Clock from './Clock';
import { useNavigation } from '@react-navigation/native';
import Options from './Options';
import getCurrentUserSelecao from './GetCurrentUserSelecao';
import React, { useEffect } from 'react';


export default function Home() {
  const diasDaSemana = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'];
  const dataAtual = new Date();
  const diaDaSemana = diasDaSemana[dataAtual.getDay()];
  const navigation = useNavigation();
  navigation.setOptions({
    headerShown: false
  })

  const handleGetCurrentUserSelecao = async() =>{
    getCurrentUserSelecao();
  }

  useEffect(()=>{
    handleGetCurrentUserSelecao()
  },[]);

  return (
    <View style={styles.container}>
      <StatusBar
          barStyle={'light-content'}
          backgroundColor={'#2f3330'}
      />
      <Image style={styles.logo} source={require('../../../assets/fds.png')} />
      <View style={styles.infoPanel}>
        <Text style={{ ...styles.panelText, padding: 10 }}>Olá, como deseja continuar?</Text>
        <Text style={{ ...styles.panelText, ...styles.daysInfo, ...styles.dayBorder }}>{diaDaSemana}</Text>
        <Clock style={{ ...styles.panelText, ...styles.daysInfo, ...styles.clockBorder }} />
      </View>
      <Options />
    </View>
  );
}







const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#2f3330",

  },
  infoPanel: {
    backgroundColor: "#176ea4",
    alignItems: "center",
    padding: 15,
    borderRadius: 20
  },
  logo: {
    width: 300,
    height: 100,
    marginTop: 60,
    marginBottom: 20
  },
  panelText: {
    color: "white",
    fontSize: 22
  },
  daysInfo: {
    backgroundColor: "black",
    padding: 10,
    borderRadius: 15,
    width: 300,
    textAlign: 'center'
  },
  clockBorder: {
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    marginTop:5
  },
  dayBorder: {
    marginTop: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  }
})