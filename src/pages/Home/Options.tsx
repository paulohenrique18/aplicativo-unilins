import React from 'react';
import {Linking, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import { Feather } from '@expo/vector-icons';
import { navigate } from '../../../App';
const Options = () => {

    const handleProfileButtonClick = () =>{
        navigate('Profile',{})
    }

    const handleClassesButtonClick = () =>{
        navigate('Classes',{});
    }

    const handleGradesButtonClick = () =>{
        navigate('Grades',{});
    }

    const handleMapButtonClick = () =>{
       Linking.openURL('https://unilins.edu.br/wp-content/uploads/2022/04/mapa_campus-04-22-1-scaled-2048x1228.jpg')
    }



    return (
        <ScrollView horizontal={false} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.container}>
            <TouchableOpacity style={styles.button} onPress={()=>{handleClassesButtonClick()}}>
                <Text style={styles.text}>Aulas de hoje</Text>
                <Feather name="book-open" size={24} color={"white"} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={()=>{handleProfileButtonClick()}}>
                <Text style={styles.text}>Perfil</Text>
                <Feather name="user" size={24} color={"white"} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={()=>{handleGradesButtonClick()}}>
                <Text style={styles.text}>Notas</Text>
                <Feather name="archive" size={24} color={"white"} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={()=>{handleMapButtonClick()}}>
                <Text style={styles.text}>Mapa do Campus</Text>
                <Feather name="map" size={24} color={"white"} />
            </TouchableOpacity>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        marginTop: 20,
        height:450
    },
    button: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        backgroundColor: '#176ea4',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 10,
        marginRight: 10,
        width: 300,
        height: 100,
        marginBottom: 20
    },
    text: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: "center"
    },
});

export default Options;
