import { useNavigation } from "@react-navigation/native";
import { api } from "../../Resources/networkConfig";
import { navigate, resetNavigation } from "../../../App";
import { Alert } from "react-native";
import { AxiosRequestConfig } from "axios";

export default async function handleLogin(user: string, password: string) {
    api.interceptors.response.use(
        response => response,
        error => {
          if (error.response.status === 401) {
            // Redireciona para a tela de login

            return;
          }
          return Promise.reject(error);
        },
      );
    const loginPath = `/getCurrentUser/${user}/${password}`
    try {
        const result = await api.get(loginPath);
        const response = {
            status: result.status,
            data: result.data,
            headers:result.headers
        }
        api.interceptors.request.use(config => {
          const auth = response.headers;
          config.headers['CorporePrincipal'] = auth['corporeprincipal'];
          config.headers['.ASPXAUTH'] = auth['.aspxauth'];
          config.headers['DefaultAlias'] = auth['defaultalias'];
          
          return config;
        });
        return response;
    } catch (e) {
        
    }
}