import { useNavigation } from "@react-navigation/native";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    Image,
    StatusBar,
    ActivityIndicator,
    Alert
} from "react-native";
import { Feather } from '@expo/vector-icons';
import React, { useEffect, useRef, useState } from "react";
import handleLogin from "./API";
import {navigate, resetNavigation} from "../../../App";

export default function LoginPage(){
    const [invalidUser,setInvalidUser]=useState(false);
    const [invalidPassword,setInvalidPassword]=useState(false);
    const [login,setLogin]=useState('');
    const [password,setPassword]=useState('');
    const [isLoading,setIsLoading]=useState(false);
    const [loginSuccess,setLoginSuccess]=useState(false);
    const [invalidCredentials,setInvalidCredentials]=useState(false);

    const resetAllInputs = ()=>{
        setInvalidUser(false);
        setInvalidPassword(false);
        setLogin('');
        setPassword('');
        setIsLoading(false);
        setLoginSuccess(false);
        setInvalidCredentials(false);
    }

    const handleInvalidCredentials = () =>{
        setInvalidCredentials(true)
        setInvalidUser(true);
        setInvalidPassword(true);
        setIsLoading(false);
    }

    const handleLoginChange = (text:string) =>{
        setLogin(text);
    }
    const handlePasswordChange = (text:string) =>{
        setPassword(text);
    }

    const handleLoginButtonPress = async ()=>{
        setInvalidCredentials(false);
        if(!login){
            setInvalidUser(true);
            return
        }
        if(!password){
            setInvalidPassword(true);
            return;
        }
        setInvalidUser(false);
        setInvalidPassword(false);
        setIsLoading(true);
        const result = await handleLogin(login,password);
        if(result?.status===200&&result.data){
            setIsLoading(false);
            setLoginSuccess(true);
            resetAllInputs();
            navigate('Home',{});
        }else{
            handleInvalidCredentials();
        }
    }

    const navigation = useNavigation();
    navigation.setOptions({
        headerShown:false
    })

    return (
    <View style={styles.mainContainer}>
        <StatusBar
            barStyle={'light-content'}
            backgroundColor={'#2f3330'}
        />
       <Image style={styles.logo} source={require('../../../assets/fds.png')}/>
        {
            invalidCredentials&&
            <Text style={{color:"red",marginBottom:2}}>Credenciais invalidas!</Text>
        }
       <View style={styles.inputs}>
            <View style={{...styles.loginInputContainer,borderColor:invalidUser?"red":"black"}}>
                <TextInput 
                    placeholder="Login" 
                    placeholderTextColor="white" 
                    style={{color:"white"}}
                    onChangeText={handleLoginChange}
                    value={login}
                >
                </TextInput>
            </View>
            <View style={{...styles.passwordInputContainer,borderColor:invalidPassword?"red":"black"}}>
                <TextInput 
                    placeholder="Senha" 
                    placeholderTextColor="white" 
                    style={{color:"white"}}
                    secureTextEntry={true}
                    onChangeText={handlePasswordChange}
                    value={password}
                >

                </TextInput>
            </View>
            {
                isLoading ?
                    <View>
                        <ActivityIndicator size="large" color="white" style={{marginTop: 30}}/>
                    </View>
                    :
                    <View style={styles.loginButtonContainer}>
                        <TouchableOpacity style={styles.loginButton} onPress={handleLoginButtonPress}>
                            <Feather name="log-in" size={24} color="white"/>
                            <Text style={styles.loginButtonText}>Login</Text>
                        </TouchableOpacity>
                    </View>
            }
            {
                loginSuccess&&
                <View style={{alignItems:"center",marginTop:20}}>
                    <Text style={{color:"white",paddingBottom:20}}>Redirecionando...</Text>
                    <Feather 
                        name="check-circle" 
                        size={38} 
                        color={"green"}
                    />
                </View>
            }
       </View>
    </View>
    )
}
const styles = StyleSheet.create({
    mainContainer:{
        backgroundColor:"#2f3330",
        flex:1,
        flexDirection:"column",
        justifyContent:"center",
        paddingBottom:50,
        alignItems:"center"
    },
    inputs:{
        width:250,
        height:200,
        justifyContent:"center",
    },
    logo:{
        width:300,
        height:130,
        marginBottom:60
    },
    loginButtonContainer:{
        backgroundColor:"gray",
        padding:15,
        borderRadius:10,
        
    },
    loginButton:{
        flexDirection:"row-reverse",
        alignItems:"center",
        justifyContent:"space-between",
    },
    loginButtonText:{
        color:"white"
    },
    loginInputContainer:{
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5,
        padding: 10,
    },
    passwordInputContainer:{
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5,
        padding: 10,
        marginTop:20,
        marginBottom:20
    }
})