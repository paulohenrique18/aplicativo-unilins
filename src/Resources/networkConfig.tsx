import axios from "axios";
import endpoints from '../Resources/endpoints.json'

const serverUrl = endpoints.mainServerUrl;
export const api = axios.create({
  baseURL: serverUrl,
  headers: {
      'Content-Type': 'application/json',
  },
});

