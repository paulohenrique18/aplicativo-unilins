import { NavigationContainer, NavigationContainerRef, StackActions, useNavigation } from "@react-navigation/native";
import { StackNavigationProp, createStackNavigator } from '@react-navigation/stack';
import LoginPage from "./src/pages/Login";
import React from "react";
import Header from "./src/components/Header";
import Home from "./src/pages/Home";
import Profile from "./src/pages/Home/ProfilePage/Profile";
import Classes from "./src/pages/Home/ClassesPage/Classes";
import Map from "./src/pages/Home/MapPage/Map";
import Grades from "./src/pages/Home/Grades/Grades";
import {Platform, StatusBar} from "react-native";
const Stack = createStackNavigator();
export const navigationRef = React.createRef<NavigationContainerRef<any>>();


export function navigate(name: string, params: any) {
  navigationRef.current?.navigate(name, params);
}

export function resetNavigation() {
  navigationRef.current?.reset({
    index: 0,
    routes: [{ name: 'LoginPage' }], // substitua "Home" pelo nome da sua tela inicial
  });
}

export default function App() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator initialRouteName="LoginPage">
        <Stack.Screen name="LoginPage" component={LoginPage}/>
        <Stack.Screen name="Home" component={Home}/>
        <Stack.Screen name="Profile" component={Profile}/>
        <Stack.Screen name={"Classes"} component={Classes}/>
        <Stack.Screen name={"Map"} component={Map}/>
        <Stack.Screen name={"Grades"} component={Grades} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

