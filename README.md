# Unilins App de Celular Open-Source

Este é um projeto de celular open-source criado por <strong>Paulo Neves</strong>, um aluno de engenharia da computação que utiliza <strong>Typescript</strong>, <strong>Expo</strong> e <strong>React Native</strong>.

## Sobre o Projeto

Este projeto é um aplicativo móvel que foi desenvolvido utilizando a plataforma Expo, com a linguagem de programação Typescript e o framework React Native. O objetivo do projeto é fornecer uma solução simples e eficiente .

## Como Instalar

Para instalar o aplicativo, siga os seguintes passos:

1. Clone o repositório em sua máquina local.
2. Abra um terminal na pasta do projeto e execute o comando `npm install` para instalar as dependências.
3. Execute o comando `expo start` para iniciar o servidor local do Expo.
4. Baixe o aplicativo "Expo" em seu celular.
5. Com o aplicativo "Expo" aberto, escaneie o QR Code gerado pelo servidor local do Expo.
6. O aplicativo será aberto em seu celular.

## Como Contribuir

Toda contribuição de alunos será bem-vinda! Se você gostaria de contribuir com o desenvolvimento deste projeto, siga os seguintes passos:

1. Faça um fork deste repositório.
2. Crie uma branch com a sua feature ou correção de bug: `git checkout -b minha-feature-ou-correcao`.
3. Faça o commit das suas alterações: `git commit -m 'Minha nova feature'`.
4. Envie suas alterações para o seu repositório fork: `git push origin minha-feature-ou-correcao`.
5. Crie um novo Pull Request para este repositório original.

## Licença

Este projeto está licenciado sob a <a href="https://opensource.org/licenses/MIT">Licença MIT</a> - veja o arquivo <strong>LICENSE.md</strong> para mais detalhes.
